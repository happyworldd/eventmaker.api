package ru.event.maker.controller;


import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.event.maker.dto.Events;
import ru.event.maker.freemaker.TemplateUtils;
import ru.event.maker.utils.JSONParser;

import java.io.IOException;


@RestController("/events")
public class EventController {


    private static final Logger LOGGER = LoggerFactory.getLogger(EventController.class);

    @Autowired
    private TemplateUtils templateUtils;

    @RequestMapping("/getEvent")
    public String getEvent() throws IOException, TemplateException {
        LOGGER.info("Зашли в метод получения события");
        Events eventsDto = JSONParser.parseJSON("events");
        return templateUtils.getScreen(eventsDto);
    }

    @RequestMapping("/getEvents/{userId}")
    public String getMyEvents(@PathVariable String userId) {
        LOGGER.info("Зашли в метод получения Моих событий");
        if (userId.isEmpty()) {
            throw new NullPointerException("Id пользователя не пришло с мобилки");
        }

        //Запрос к базе за получением событий


        return null;
    }

    @RequestMapping("/getInvitationEvents/{userId}")
    public String getInvitationEvents(@PathVariable String userId) {
        if (userId.isEmpty()) {
            throw new NullPointerException("Id пользователя не пришло с мобилки");
        }

        //Запрос к базе за получением приглашенных событий
        return null;
    }




}


