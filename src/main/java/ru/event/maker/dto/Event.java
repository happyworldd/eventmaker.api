package ru.event.maker.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Event {
    private String id;
    private String type;
    private String description;
    private String place;
    private String creator;
    private String creationDate;
    private String date;
    private String playersAmount;
}
