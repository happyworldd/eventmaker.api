package ru.event.maker.freemaker;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import org.springframework.stereotype.Component;
import ru.event.maker.dto.Events;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Locale;

@Component
public class TemplateUtils {

    private final String EVENTS = "/templates/events.ftl";

    private Configuration cfg;


    public TemplateUtils() {
        cfg = new Configuration();

        cfg.setClassForTemplateLoading(this.getClass(), "/");
        cfg.setIncompatibleImprovements(new Version(2, 3, 20));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setLocale(Locale.US);

    }

    private String renderToString(Object dto, String templateName) throws IOException, TemplateException {
        Template template = cfg.getTemplate(templateName);
        StringWriter stringWriter = new StringWriter();
        template.process(dto, stringWriter);
        String result = stringWriter.toString();
        System.out.println("Результат создания JSON: " + result);
        return result;
    }


    public String getScreen(Object dto) throws IOException, TemplateException {
        String templateName = "";
        if (dto instanceof Events) {
            templateName = EVENTS;
        }
        return renderToString(dto, templateName);
    }
}
