package ru.event.maker.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import ru.event.maker.dto.Events;

import java.io.IOException;
import java.net.URL;

public class JSONParser {
    public static Events parseJSON(String stubName) {

        ObjectMapper objectMapper = new ObjectMapper();

        String filenameTemplate = "stubs/%s.json";
        URL resource;
        try {
            resource = Resources.getResource(String.format(filenameTemplate, stubName));
        } catch (IllegalArgumentException ex) {
            System.out.println("Не удалось прочитать файл");
            resource = null;
        }
        Events result = null;
        try {
            result = objectMapper.readValue(resource, Events.class);
        } catch (IOException e) {
            System.out.println("Ошибка при десериализации!");
            e.printStackTrace();
        }
        return result;
    }
}
