
{
  "eventsList" : [

    <#list eventsList as item>
     {
          "id" : "${item.id}",
          "type" : "${item.type}",
          "description": "${item.description}",
          "place" : "${item.place}",
          "creator": "${item.creator}",
          "date" : "${item.date}",
          "playersAmount": "${item.playersAmount}"
        }
        <#sep>,
    </#list>
  ]
}
